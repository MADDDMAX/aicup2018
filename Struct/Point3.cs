using System;
using Ext;

namespace Struct {
	public struct Point3 {
		public double X, Y, Z;

		public Point3(double x, double y, double z) {
			this.X = x;
			this.Y = y;
			this.Z = z;
		}

		public Point3(Point2 p) {
			this.X = p.X;
			this.Y = p.Y;
			this.Z = 0;
		}

		public override string ToString() {
			return X.ToString() + ' ' + Y.ToString() + ' ' + Z.ToString();
		}

		public static Point3 operator +(Point3 q, Point3 w) {
			return new Point3(q.X + w.X, q.Y + w.Y, q.Z + w.Z);
		}

		public static Point3 operator -(Point3 q, Point3 w) {
			return new Point3(q.X - w.X, q.Y - w.Y, q.Z - w.Z);
		}

		public static Point3 operator *(Point3 q, double w) {
			return new Point3(q.X * w, q.Y * w, q.Z * w);
		}

		public static Point3 operator /(Point3 q, double w) {
			return new Point3(q.X / w, q.Y / w, q.Z / w);
		}

		public static double operator *(Point3 q, Point3 w) {
			return q.X * w.X + q.Y * w.Y + q.Z * w.Z;
		}

		public bool Eq(Point3 p) {
			return this.Dist(p).Eq(0);
		}

		public bool Between(Point3 q, Point3 w) {
			return this.X.Between(q.X, w.X) && this.Y.Between(q.Y, w.Y) && this.Z.Between(q.Z, w.Z);
		}

		public double Dist() {
			return Math.Sqrt(this.X * this.X + this.Y * this.Y + this.Z * this.Z);
		}

		public double DistSqr() {
			return this.X * this.X + this.Y * this.Y + this.Z * this.Z;
		}

		public double Dist(Point3 p) {
			var q = this - p;
			return q.Dist();
		}

		public Point3 Normalize() {
			var d = Dist();
			if (d.Eq(0)) {
				return new Point3(0, 0, 0);
			}
			return new Point3(this.X / d, this.Y / d, this.Z / d);
		}

		public bool CloseTo(Point3 p, double d = 0.1) {
			return this.Dist(p).Less(d);
		}

		public Point3 Clamp(double l) {
			if (this.Dist().More(l)) {
				return this.Normalize() * l;
			}
			return this;
		}
	}
}