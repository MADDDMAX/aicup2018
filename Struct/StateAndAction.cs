﻿using Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk.Model.MiXa;

namespace Struct {
	class StateAndAction {
		public Entity state;
		public Action action;

		public StateAndAction(Entity state, Action action) {
			this.state = new Entity(state);
			this.action = action;
		}
	}
}
