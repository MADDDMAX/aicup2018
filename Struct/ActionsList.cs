﻿using System.Collections.Generic;

namespace Struct {
	class ActionsList
    {
		private int startTick;
		private List<StateAndAction> actions;
		public int LastTick { get { return startTick + actions.Count - 1; } }

		public ActionsList(int currentTick, List<StateAndAction> futureActions) {
			startTick = currentTick;
			actions = futureActions;
		}

		public StateAndAction GetAction(int tick) {
			if (tick < startTick || tick >= startTick + actions.Count) {
				return null;
			}
			return actions[tick - startTick];
		}
    }
}
