﻿namespace Struct {
	public class Action {
		public Point3 TargetVelocity { get; set; }
		public double JumpSpeed { get; set; }
		public bool UseNitro { get; set; }
	}
}
