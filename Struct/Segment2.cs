namespace Struct {
	public struct Segment2 {
		public Point2 P1, P2;

		public Segment2(Point2 p, Point2 q) {
			P1 = p;
			P2 = q;
		}

		public double Dist(Point2 p) {
			Point2 v = this.P2 - this.P1;
			Point2 w = p - this.P1;
			double c1 = w * v;
			if (c1 <= 0) {
				return (p - this.P1).Dist();
			}
			double c2 = v * v;
			if (c2 <= c1) {
				return (p - this.P2).Dist();
			}
			double b = c1 / c2;
			Point2 Pb = this.P1 + v * b;
			return (p - Pb).Dist();
		}
	}
}