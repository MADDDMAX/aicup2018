using System.Dynamic;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Render {
	public abstract class Figure {
		public string Json() {
			var figureName = this.GetType().Name;
			var obj = new ExpandoObject();
			IDictionary<string, object> objDict = (IDictionary<string, object>)obj;
			objDict.Add(figureName, this);
			var json =  JsonConvert.SerializeObject(objDict);
			return json;
		}
	}
}