using Struct;

namespace Render {
	public class Line : Figure {
		public double x1, y1, z1, x2, y2, z2, width, r, g, b, a;
		public Line(Point3 p1, Point3 p2, double width = 1, double r = 1, double g = 0, double b = 0, double a = 0.5) {
			this.x1 = p1.X;
			this.y1 = p1.Z;
			this.z1 = p1.Y;
			this.x2 = p2.X;
			this.y2 = p2.Z;
			this.z2 = p2.Y;
			this.width = width;
			this.r = r;
			this.g = g;
			this.b = b;
			this.a = a;
		}
	}
}