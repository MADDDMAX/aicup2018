namespace Render {
	public class Text : Figure {
		public string text;
		public Text(string text) {
			this.text = text;
		}
		public new string Json() {
			return "{\"Text\":\"" + text + "\"}";
		}
	}
}