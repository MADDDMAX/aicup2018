#define noRENDER
using Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk.Model;
using MiXa = Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk.Model.MiXa;
using System;
using System.Collections.Generic;
using System.Linq;
using Struct;
using Ext;
using Render;
using System.Diagnostics;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk {
	public sealed class MyStrategy : IStrategy {
		private bool render = false;
		private MiXa.Robot me;
		private Rules rules;
		private Game game;
		private Model.Action action;
		private MiXa.Ball ball;
		private MiXa.Ball ballNext;
		private MiXa.Robot[] robots;
		private MiXa.Robot[] myRobots;
		private MiXa.Robot[] hisRobots;
		private MiXa.NitroPack[] nitroPacks;
		private int tick = -1;
		private Rand rand;
		private Simulation simulation;
		private Dictionary<int, ActionsList> actionsStore;
		private Dictionary<int, Dictionary<string, int>> tickToStartAction;
		private Dictionary<int, int> useNitroTill = new Dictionary<int, int>();
		private Dictionary<double, Dictionary<bool, Simulator>> canHitBallStore;

		private int goalsCount = -1;
		private int lastGoal = -1;
		private int lastJump = -1;
		private Dictionary<int, int> goals = new Dictionary<int, int>();
		private bool firstEverRun = true;
		private bool tickFirstRun = true;
		private bool timeOut = false;
		private int goalkeeperId = -1;
		private int middleId = -1;
		private Point2 myGoal;
		private Point2 hisGoal;
		private double[] jumpHeights = new double[31];
		private double[] jumpHeightsNitro = new double[31];

		private Stopwatch timer;
		private Stopwatch timerInit;
		private Stopwatch[] timersRobots = new Stopwatch[3];
		private Stopwatch[] timers = new Stopwatch[3];
		private List<Text> renderText = new List<Text>();
		private List<Sphere> renderSphere = new List<Sphere>();
		private List<Line> renderLine = new List<Line>();

		private int GetGoalsCount() {
			int result = 0;
			for (int i = 0; i < game.players.Length; i++) {
				result += game.players[i].score;
				if (game.players[i].score != goals[game.players[i].id]) {
					Console.WriteLine(tick.ToString() + ": --" + (game.players[i].me ? "Yes" : "No"));
				}
				goals[game.players[i].id] = game.players[i].score;
			}

			return result;
		}

		private void InitOneTime() {
#if RENDER
			render = true;
#endif
			firstEverRun = false;
			rand = new Rand((int)rules.seed);
			timer = new Stopwatch();
			timerInit = new Stopwatch();
			timers = timers.Select(x => new Stopwatch()).ToArray();
			timersRobots = timersRobots.Select(x => new Stopwatch()).ToArray();
			simulation = new Simulation(me.PlayerId, rules);
			jumpHeights[0] = 1;
			for (int i = 1; i < jumpHeights.Length; i++) {
				jumpHeights[i] = Consts.ROBOT_MAX_RADIUS + 0.24985 * (i - 0.01) - (Consts.GRAVITY * i * i / 2);
			}
			jumpHeightsNitro[0] = 1;
			for (int i = 1; i < jumpHeightsNitro.Length; i++) {
				jumpHeightsNitro[i] = Consts.ROBOT_MAX_RADIUS + 0.24985 * (i - 0.01);
			}
			for (int i = 0; i < game.players.Length; i++) {
				goals[game.players[i].id] = 0;
			}
			myGoal = new Point2(0, -rules.arena.depth / 2);
			hisGoal = new Point2(0, rules.arena.depth / 2);
		}

		private void InitNewTick() {
			tickFirstRun = true;
			ball = new MiXa.Ball(game.ball);
			ballNext = new MiXa.Ball(game.ball);
			ballNext.Pos += ballNext.Speed;
			robots = game.robots.Select(x => new MiXa.Robot(x)).ToArray();
			var goalkeeper = robots.Where(x => x.Id == goalkeeperId).First();
			if (!timeOut && tick - lastGoal > 50 && (goalkeeper.Y > -rules.arena.depth / 3 || goalkeeper.Z > rules.arena.height / 3)) {
				myRobots = robots.Where(x => x.IsTeammate).ToArray();
				var newGoalkeeper = myRobots.Where(x => x.Touch || x.Id == goalkeeperId).OrderBy(x => x.Pos2.Dist(myGoal)).First();
				if (newGoalkeeper != null && newGoalkeeper.Id != goalkeeperId) {
					if (newGoalkeeper.Id == middleId) {
						middleId = goalkeeper.Id;
					}
					goalkeeperId = newGoalkeeper.Id;
					if (render) {
						Console.WriteLine(tick.ToString() + " sw");
					}
				}
			}
			myRobots = robots.Where(x => x.IsTeammate).OrderBy(x => x.Id == goalkeeperId ? -1 : x.Id == middleId ? 999999 : x.Id).ToArray();
			hisRobots = robots.Where(x => !x.IsTeammate).ToArray();
			nitroPacks = game.nitro_packs.Select(x => new MiXa.NitroPack(x)).ToArray();

			tick = game.current_tick;
			if (tick == rules.max_tick_count - 1) {
				Console.WriteLine("i." + timerInit.Elapsed);
				Console.WriteLine("a." + timer.Elapsed);
				Console.WriteLine("1." + timersRobots[0].Elapsed);
				Console.WriteLine("2." + timersRobots[1].Elapsed);
				Console.WriteLine("3." + timersRobots[2].Elapsed);
				Console.WriteLine(timers[0].Elapsed);
				Console.WriteLine(timers[1].Elapsed);
				Console.WriteLine(timers[2].Elapsed);
				if (render) {
					Console.ReadLine();
				}
			}
			var reset = simulation.Update(game);
			if (reset) {
				actionsStore = new Dictionary<int, ActionsList>();
				tickToStartAction = new Dictionary<int, Dictionary<string, int>>();
			}
			renderText.Clear();
			renderSphere.Clear();
			renderLine.Clear();
			for (int i = tick; i < tick + simulation.TicksCount; i++) {
				var snapshot = simulation.GetTick(i);
				if (render && snapshot != null) {
					renderSphere.Add(new Sphere(snapshot.Ball.X, snapshot.Ball.Y, snapshot.Ball.Z, 2, 1, 0, 0, 0.1));
				}
			}
		}

		private void InitAfterGoal() {
			timeOut = false;
			lastGoal = tick;
			goalsCount = GetGoalsCount();
			var robotsByDist = game.robots.Where(x => x.is_teammate).OrderBy(x => Math.Abs(x.x)).Select(x => x.id).ToArray();
			goalkeeperId = robotsByDist.Last();
			if (robotsByDist.Length == 3) {
				middleId = robotsByDist[1];
			}
		}

		private void Init(Model.Robot _me, Rules _rules, Game _game, Model.Action _action) {
			me = new MiXa.Robot(_me);
			rules = _rules;
			game = _game;
			action = _action;

			if (firstEverRun) {
				InitOneTime();
			}

			timerInit.Start();
			if (GetGoalsCount() > goalsCount) {
				timeOut = true;
				if (game.ball.x.Eq(0) && game.ball.z.Eq(0)) {
					InitAfterGoal();
				}
			}

			tickFirstRun = false;
			if (game.current_tick > tick) {
				InitNewTick();
			}
			timerInit.Stop();
		}

		private bool IsInGoalMouth(Point3 p) {
			var goalMouthR = rules.arena.goal_width / 2 + ball.R;
			var goal = new Point3(myGoal.X - 5, myGoal.Y + ball.R, ball.R);
			for (int i = 0; i < 3; i++) {
				if (p.Dist(goal) < goalMouthR) {
					return true;
				}
				goal += new Point3(5, 0, 0);
			}
			return false;
		}

		private Simulator BallIsGoingToGoalMouth() {
			var tickMax = 40;
			if (ball.Y.More(0) || (ball.Y.More(-rules.arena.goal_depth / 4) && ball.Speed.Y.More(0.1))) {
				return null;
			}
			var simulator = new Simulator(rules.arena, new MiXa.Robot[0], ball, 1);
			while (simulator.Ticks < tickMax && !IsInGoalMouth(simulator.Ball.Pos)) {
				simulator.Update();
			}
			while (simulator.Ticks < tickMax && simulator.Ball.Pos.Z.More(Consts.BALL_RADIUS + Consts.ROBOT_RADIUS * 2)) {
				simulator.Update();
			}
			if (simulator.Ticks < tickMax && IsInGoalMouth(simulator.Ball.Pos) && simulator.Ball.Speed.Y.Less(0.1)) {
				return simulator;
			} else {
				return null;
			}
		}

		private Point2 GetTargetVelocity(MiXa.Robot robot, Point2 p, bool stop = false) {
			if (!robot.Touch) {
				return new Point2(0, 0);
			}
			Point2 velocity;
			var simulator = new Simulator(rules.arena, game, 1);
			MiXa.Robot robotSimulation = simulator.GetRobot(robot.Id);
			robotSimulation.R = Consts.ROBOT_MIN_RADIUS;
			while (!robotSimulation.Touch) {
				simulator.Update();
			}
			int ticksToStop = Math.Max(1, (int)Math.Ceiling(robot.Speed.Dist() / Consts.ROBOT_ACCELERATION / 1.5)); // /1.5 to simulate slowdown
			if (stop) {
				simulator.Update(ticksToStop);
				velocity = (p - robotSimulation.Pos2) / ticksToStop;
			} else if (robot.Speed.Dist() > Consts.ROBOT_ACCELERATION) {
				velocity = p - (robot.Pos2 + robot.Speed2);
				velocity = velocity.Normalize();
			} else {
				velocity = p - robot.Pos2;
				velocity = velocity.Normalize();
			}
			return velocity;
		}

		private Point3 GetUpdatedSpeed(MiXa.Robot robot, Point3 target_velocity, bool nitro = false) {
			var result = robot.Speed;
			if (robot.Touch) {
				target_velocity = target_velocity.Clamp(Consts.ROBOT_MAX_GROUND_SPEED);
				target_velocity -= robot.TouchNormal * (robot.TouchNormal * target_velocity);
				var target_velocity_change = target_velocity - robot.Speed;
				if (target_velocity_change.Dist().More(0)) {
					var acceleration = Consts.ROBOT_ACCELERATION * Math.Max(0, robot.TouchNormal.Z);
					result += (target_velocity_change.Normalize() * acceleration).Clamp(target_velocity_change.Dist());
				}
			}
			if (nitro) {
				var target_velocity_change = (target_velocity - robot.Speed).Clamp(robot.NitroAmount * Consts.NITRO_POINT_VELOCITY_CHANGE);
				if (target_velocity_change.Dist() > 0) {
					var acceleration = target_velocity_change.Normalize() * Consts.ROBOT_NITRO_ACCELERATION;
					var velocity_change = acceleration.Clamp(target_velocity_change.Dist());
					robot.Speed += velocity_change;
					robot.NitroAmount -= velocity_change.Dist() / Consts.NITRO_POINT_VELOCITY_CHANGE;
				}
			}
			return result;
		}

		private bool CanFullStopAndReach(MiXa.Robot robot, Point2 p, int tickLimit, int ticksForJump) {
			var V0 = robot.Speed.Dist();
			var a = Consts.ROBOT_ACCELERATION;
			var ticksToStop = (int)Math.Ceiling(V0 / a);
			var vectToStop = robot.Speed.Normalize() * robot.Speed.DistSqr() / 2 / a;
			var PosAfterStop = new Point2(robot.Pos + vectToStop);
			var ticksToGo = tickLimit - ticksToStop - ticksForJump;
			if (ticksToGo < 0) {
				return false;
			}
			var ticksToAccelerate = Math.Min(ticksToGo, (int)Math.Round(Consts.ROBOT_MAX_GROUND_SPEED / Consts.ROBOT_ACCELERATION));
			var ticksMaxSpeed = ticksToGo - ticksToAccelerate + ticksForJump;
			var SAccelerate = a * ticksToAccelerate.Sqr() / 2;
			var SMaxSpeed = ticksMaxSpeed * a * ticksToAccelerate;
			return SAccelerate + SMaxSpeed > PosAfterStop.Dist(p) + Consts.ROBOT_RADIUS / 3;
		}

		private int TicksToWait(MiXa.Robot robot, Point2 p, int tickLimit, int ticksForJump) {
			var result = 0;
			var a = Consts.ROBOT_ACCELERATION;
			var ticksToGo = tickLimit - ticksForJump;
			while (ticksToGo > 0) {
				var ticksToAccelerate = Math.Min(ticksToGo, (int)Math.Round(Consts.ROBOT_MAX_GROUND_SPEED / Consts.ROBOT_ACCELERATION));
				var ticksMaxSpeed = ticksToGo - ticksToAccelerate + ticksForJump;
				var SAccelerate = a * ticksToAccelerate.Sqr() / 2;
				var SMaxSpeed = ticksMaxSpeed * a * ticksToAccelerate;
				if (SAccelerate + SMaxSpeed > robot.Pos2.Dist(p) + Consts.ROBOT_RADIUS / 3) {
					result++;
				} else {
					break;
				}
				ticksToGo--;
			}
			result--;
			return result;
		}

		private int TicksForJump(MiXa.Robot robot, double Z, bool useNitro = false) {
			if (!useNitro) {
				var ticksForJump = 1;
				while (ticksForJump < jumpHeights.Length && jumpHeights[ticksForJump] < Z) {
					ticksForJump++;
				}
				return ticksForJump;
			} else {
				if (!robot.CanUseNitro) {
					return jumpHeightsNitro.Length;
				}
				var nitroTicks = Math.Max(Consts.MAX_NITRO_TICKS, (int)Math.Floor(robot.NitroAmount / Consts.NITRO_PER_TICK));
				var ticksForJump = 1;
				while (ticksForJump <= nitroTicks && jumpHeightsNitro[ticksForJump] < Z) {
					ticksForJump++;
				}
				if (ticksForJump > nitroTicks) {
					while (ticksForJump < jumpHeights.Length && jumpHeightsNitro[nitroTicks] + jumpHeights[ticksForJump - nitroTicks] - Consts.ROBOT_MAX_RADIUS < Z) {
						ticksForJump++;
					}
				}
				return ticksForJump;
			}
		}

		private Tuple<int, List<StateAndAction>> TickToReach(MiXa.Robot robot, Point3 p, int tickLimit, int microTicks = Consts.MICROTICKS_PER_TICK, bool useNitro = false) {
			var emptyActions = Tuple.Create<int, List<StateAndAction>>(1000, null);
			var actions = new List<StateAndAction>();
			var ticksForJump = TicksForJump(robot, p.Z, useNitro);
			if (ticksForJump >= jumpHeights.Length) {
				return useNitro ? emptyActions : TickToReach(robot, p, tickLimit, microTicks, true);
			}
			var pGround = new Point2(p);
			var simulator = new Simulator(rules.arena, new MiXa.Robot[] { robot }, null, microTicks);
			MiXa.Robot robotSimulation = simulator.GetRobot(robot.Id);
			robotSimulation.R = Consts.ROBOT_RADIUS;
			if (robot.Y < p.Y) {
				if (CanFullStopAndReach(robotSimulation, pGround, tickLimit, ticksForJump)) {
					while (robotSimulation.Speed.Dist() > Consts.ROBOT_ACCELERATION / 10) {
						robotSimulation.Action = new Struct.Action();
						robotSimulation.Action.TargetVelocity = new Point3(0, 0, 0);
						actions.Add(new StateAndAction(robotSimulation, robotSimulation.Action));
						simulator.Update();
					}
					var ticksToWait = TicksToWait(robotSimulation, pGround, tickLimit - actions.Count, ticksForJump);
					for (int _ = 0; _ < ticksToWait; _++) {
						robotSimulation.Action = new Struct.Action();
						robotSimulation.Action.TargetVelocity = new Point3(0, 0, 0);
						actions.Add(new StateAndAction(robotSimulation, robotSimulation.Action));
						simulator.Update();
					}
					return Tuple.Create(tickLimit, actions);
				}
			}
			var i = 0;
			var straightFirstTime = true;
			while (i <= tickLimit - ticksForJump && !robotSimulation.Pos.CloseTo(p, Consts.ROBOT_RADIUS / 3) && !(robotSimulation.Pos2.CloseTo(pGround, Consts.ROBOT_RADIUS / 3) && p.Z < Consts.ROBOT_RADIUS * 2.5)) {
				var sourcePoint = robotSimulation.Pos2;
				if (i < (tickLimit - ticksForJump) * 2 / 3) {
					sourcePoint += robotSimulation.Speed2 / 2;
				}
				if (i < (tickLimit - ticksForJump) / 3) {
					sourcePoint += robotSimulation.Speed2 / 2;
				}
				robotSimulation.Action = new Struct.Action();
				robotSimulation.Action.TargetVelocity = new Point3(pGround - sourcePoint) / (tickLimit - i);

				var straight = robotSimulation.Speed.Dist() > Consts.ROBOT_ACCELERATION && (robotSimulation.Action.TargetVelocity.Normalize() - robotSimulation.Speed.Normalize()).Dist() < Consts.ROBOT_RADIUS / 3 / ticksForJump;
				if (straight) {
					if (straightFirstTime && robot.Y < p.Y) {
						straightFirstTime = false;
						var ticksOnFloorLeft = tickLimit - ticksForJump - i;
						var distToP = robotSimulation.Pos2.Dist(pGround);
						var V0 = robotSimulation.Speed.Dist();
						var ticksToSlowDown = 1;
						var a = Consts.ROBOT_ACCELERATION;
						while (ticksToSlowDown < ticksOnFloorLeft) {
							var t0 = Math.Min(ticksToSlowDown, V0 / a);
							var t = Math.Min(ticksOnFloorLeft - ticksToSlowDown, (int)Math.Round(Consts.ROBOT_MAX_GROUND_SPEED / Consts.ROBOT_ACCELERATION));
							var V1 = V0 - a * t0;
							t = Math.Min(t, (int)Math.Round((Consts.ROBOT_MAX_GROUND_SPEED - V1) / Consts.ROBOT_ACCELERATION));
							var tMaxSpeed = ticksOnFloorLeft - ticksToSlowDown - t;
							var S1 = V0 * t0 - a * t0.Sqr() / 2;
							var S2 = V1 * t + a * t.Sqr() / 2;
							var S3 = (V1 + a * t) * ticksForJump;
							var S4 = Consts.ROBOT_MAX_GROUND_SPEED * tMaxSpeed;
							var S = S1 + S2 + S3 + S4;
							if (S.Less(distToP)) {
								break;
							}
							ticksToSlowDown++;
						}
						ticksToSlowDown--;
						if (ticksToSlowDown > 0) {
							while (ticksToSlowDown > 0) {
								robotSimulation.Action.TargetVelocity = new Point3(0, 0, 0);
								simulator.Update();
								actions.Add(new StateAndAction(robotSimulation, robotSimulation.Action));
								ticksToSlowDown--;
								i++;
							}
							continue;
						}
					}
					var posAfterJump = robotSimulation.Pos2 + robotSimulation.Speed2 * ticksForJump;
					if (posAfterJump.CloseTo(pGround, Consts.ROBOT_RADIUS / 3)) {
						return Tuple.Create(i + ticksForJump, actions);
					} else {
						if (i + ticksForJump > tickLimit) {
							return useNitro ? emptyActions : TickToReach(robot, p, tickLimit, microTicks, true);
						}
					}
				}
				i++;
				actions.Add(new StateAndAction(robotSimulation, robotSimulation.Action));
				simulator.Update();
			}
			return i <= tickLimit - ticksForJump ? Tuple.Create(i, actions) : emptyActions;
		}

		private int TickToReach(MiXa.Robot robot, Point2 p, int tickLimit, bool stop = false) {
			if (!robot.Touch) {
				return tickLimit + 1;
			}
			var simulator = new Simulator(rules.arena, game, 1);
			MiXa.Robot robotSimulation = simulator.GetRobot(robot.Id);
			var i = 0;
			while (i <= tickLimit && !robotSimulation.Pos2.CloseTo(p, Consts.ROBOT_RADIUS / 2)) {
				i++;
				var targetVelocity = new Point3(GetTargetVelocity(robotSimulation, p, stop));
				robotSimulation.Speed = GetUpdatedSpeed(robotSimulation, targetVelocity);
				var j = 0;
				while (j < simulator.MicroTicksCount && !robotSimulation.Pos2.CloseTo(p, Consts.ROBOT_RADIUS / 2)) {
					j++;
					simulator.Update(0, 1);
				}
			}
			return i + (int)(Math.Floor(robotSimulation.Pos2.Dist(p) / Consts.ROBOT_MAX_GROUND_SPEED));
		}

		private void GoTo3(MiXa.Robot robot, Point3 p, int tickLimit = 0) {
			if (tickLimit > 0) {
				var tickToReach = TickToReach(me, p, tickLimit);
				if (tickToReach.Item1 <= tickLimit) {
					actionsStore[robot.Id] = new ActionsList(tick, tickToReach.Item2);
				} else {
					tickLimit = 0;
				}
			}
			if (tickLimit == 0) {
				var pGround = new Point2(p);
				robot.Action.TargetVelocity = new Point3(pGround - (robot.Pos2 + robot.Speed2 / 2)).Clamp(1) * Consts.ROBOT_MAX_GROUND_SPEED;
			}
		}

		private void GoTo(MiXa.Robot robot, Point2 p, bool stop = false) {
			var maxSpeedAtBorders = 0.08;
			var velocity = GetTargetVelocity(robot, p, stop);
			var normalIsVertical = robot.Touch && robot.TouchNormal.Z.Eq(1);
			if (robot.Speed.Dist().More(maxSpeedAtBorders) || !normalIsVertical) {
				var gap = robot.R * 4;
				var arenaX = rules.arena.width / 2 - gap;
				var cornerPoint = new Point2(rules.arena.width / 2 - rules.arena.corner_radius, rules.arena.depth / 2 - rules.arena.corner_radius);
				if (robot.X.More(arenaX) && velocity.X.More(0) && robot.Speed.X.More(maxSpeedAtBorders)) {
					velocity.X = 0;
				}
				if (robot.X.Less(-arenaX) && velocity.X.Less(0) && robot.Speed.X.Less(-maxSpeedAtBorders)) {
					velocity.X = 0;
				}
				if (Math.Abs(robot.X).More(cornerPoint.X) && Math.Abs(robot.Y).More(cornerPoint.Y)) {
					cornerPoint.X *= Math.Sign(robot.X);
					cornerPoint.Y *= Math.Sign(robot.Y);
					if (robot.Pos2.Dist(cornerPoint).More(rules.arena.corner_radius - gap)) {
						if (Math.Sign(velocity.X) == Math.Sign(robot.X - cornerPoint.X) && Math.Sign(velocity.X) == Math.Sign(robot.Speed.X) && Math.Abs(robot.Speed.X).More(maxSpeedAtBorders)) {
							velocity.X = 0;
						}
						if (Math.Sign(velocity.Y) == Math.Sign(robot.Y - cornerPoint.Y) && Math.Sign(velocity.Y) == Math.Sign(robot.Speed.Y) && Math.Abs(robot.Speed.Y).More(maxSpeedAtBorders)) {
							velocity.Y = 0;
						}
					}
				}
			}
			robot.Action.TargetVelocity = new Point3(velocity.X, velocity.Y, 0);
		}

		private MiXa.Robot ClosestOpponent(Point3 p) {
			return hisRobots.OrderBy(x => x.Pos.Dist(p)).First();
		}

		private MiXa.Robot ClosestOpponentInFront(Point3 p) {
			return hisRobots.Where(x => x.Y.More(p.Y)).OrderBy(x => x.Pos.Dist(p)).FirstOrDefault();
		}

		private bool CanScore(MiXa.Robot robot, Simulator simulator, double radius = Consts.ROBOT_MAX_RADIUS, int tickLimit = 80) {
			var microTicksCount = simulator.MicroTicksCount;
			simulator.SetMicroTicksCount(Consts.MICROTICKS_PER_TICK);
			MiXa.Robot robotSimulation = simulator.GetRobot(robot.Id);
			robotSimulation.R = radius;
			while (simulator.Ticks < tickLimit && simulator.Ball.Y.Less(hisGoal.Y + Consts.BALL_RADIUS / 4)) {
				simulator.Update(0, 1);
				if (simulator.Ticks == 1) {
					simulator.SetMicroTicksCount(microTicksCount);
				}
				if (simulator.Ticks > 2 && simulator.Ball.Speed.Y.Less(0)) {
					return false;
				}
				if (render && simulator.MicroTicks == 0) {
					renderSphere.Add(new Sphere(simulator.Ball.Pos, 1, 0, 0, 1, 0.3));
				}
			}
			if (simulator.Ticks < tickLimit) {
				if (simulator.Ball.Z > rules.arena.goal_height - Consts.BALL_RADIUS * 1.5) { // Hit close to the crossbar
					if (render) {
						Console.Write(tick + ": sc1 ");
					}
					return true;
				}
				if (ball.Y.More(hisGoal.Y - ball.R)) { // ball is almost there
					if (render) {
						Console.Write(tick + ": sc2 ");
					}
					return true;
				}
				var opponent = ClosestOpponent(simulator.Ball.Pos);
				var pointToReach = simulator.Ball.Pos + new Point3(Math.Sign(opponent.X - simulator.Ball.X) * 1.5, 0, -1.5);
				pointToReach.Z = Math.Min(jumpHeights[jumpHeights.Length - 1], pointToReach.Z);
				var opponentCanReach = TickToReach(opponent, pointToReach, simulator.Ticks - 1, 3).Item1 < simulator.Ticks;
				if (!opponentCanReach) { // opponent can't reach
					if (render) {
						Console.Write(tick + ": sc3 ");
					}
					return true;
				}
				if (opponent.Pos.Dist(simulator.Ball.Pos).More(simulator.Ticks * Consts.ROBOT_MAX_GROUND_SPEED)) { // opponent is too far
					if (render) {
						Console.Write(tick + ": sc4 ");
					}
					return true;
				}
				if (ball.Y.More(hisGoal.Y - ball.R * 4) && Math.Abs(ball.X) < rules.arena.goal_width / 2) { // ball is in front of goal
					if (render) {
						Console.Write(tick + ": sc5 ");
					}
					return true;
				}
				if (simulator.Ticks < 60 && Math.Abs(simulator.Ball.X) > rules.arena.goal_width / 2 - Consts.BALL_RADIUS * 1.5) { // Hit close to the post
					if (render) {
						Console.Write(tick + ": sc6 ");
					}
					return true;
				}
				if (simulator.Ticks < 60 && simulator.Ball.Speed.Dist() > Consts.ROBOT_MAX_GROUND_SPEED * 1.5) {
					if (render) {
						Console.Write(tick + ": sc7 ");
					}
					return true;
				}
				var myRobotsNearHisGoal = myRobots.Where(x => x.Y > rules.arena.depth / 2 - Consts.ROBOT_RADIUS * 5);
				var hisRobotsNearHisGoal = hisRobots.Where(x => x.Y > rules.arena.depth / 2 - Consts.ROBOT_RADIUS * 5);
				if (myRobots.Count() > 2 && myRobotsNearHisGoal.Count() == 1 && hisRobotsNearHisGoal.Count() <= 1 && simulator.Ball.Z > Consts.BALL_RADIUS * 2) {
					if (render) {
						Console.Write(tick + ": sc8 ");
					}
					return true;
				}
			}
			return false;
		}

		private Simulator CanHitBall(MiXa.Robot robot, double radius, bool useNitro = false) {
			if (canHitBallStore.ContainsKey(radius) && canHitBallStore[radius].ContainsKey(useNitro)) {
				return canHitBallStore[radius][useNitro];
			}
			if (!canHitBallStore.ContainsKey(radius)) {
				canHitBallStore[radius] = new Dictionary<bool, Simulator>();
			}
			if (robot.Pos.Dist(ball.Pos).More(Consts.BALL_RADIUS * 20)) {
				canHitBallStore[radius][useNitro] = null;
				return null;
			}
			if (useNitro && !robot.CanUseNitro) {
				canHitBallStore[radius][useNitro] = null;
				return null;
			}
			var microTicks = 20;
			var simulator = new Simulator(rules.arena, game, microTicks);
			MiXa.Robot robotSimulation = simulator.GetRobot(robot.Id);
			robotSimulation.R = radius;
			var robotNext = robotSimulation.Pos + robotSimulation.Speed;
			var ballNext = simulator.Ball.Pos + simulator.Ball.Speed;
			if (useNitro) {
				robotSimulation.Action = new Struct.Action();
				robotSimulation.Action.UseNitro = true;
				robotSimulation.Action.TargetVelocity = new Point3(0, 0, 1000);
			}
			var tickLimit = jumpHeights.Length - (Consts.ROBOT_MAX_RADIUS - radius) * 550;
			while (simulator.Ticks < tickLimit && robotNext.Dist(ballNext) > Consts.BALL_RADIUS + Consts.ROBOT_MAX_RADIUS) {
				if (simulator.Ticks > 0 && robotNext.Dist(ballNext) > robotSimulation.Pos.Dist(ballNext)) {
					canHitBallStore[radius][useNitro] = null;
					return null;
				}
				if (useNitro && simulator.Ticks == Consts.MAX_NITRO_TICKS) {
					robotSimulation.Action.UseNitro = false;
				}
				simulator.Update();
				robotSimulation.R = Consts.ROBOT_RADIUS;
				robotNext = robotSimulation.Pos + robotSimulation.Speed;
				ballNext = simulator.Ball.Pos + simulator.Ball.Speed;
			}
			var result = simulator.Ticks < tickLimit || robotNext.Dist(ballNext) < Consts.BALL_RADIUS + Consts.ROBOT_MAX_RADIUS ? simulator : null;
			canHitBallStore[radius][useNitro] = result;
			return result;
		}

		private Simulator CanHitOpponent(MiXa.Robot robot, MiXa.Robot opponent, bool useNitro = false) {
			if (robot.Pos.Dist(opponent.Pos).More(Consts.ROBOT_RADIUS * 5)) {
				return null;
			}
			if (useNitro && !robot.CanUseNitro) {
				return null;
			}
			var microTicks = 2;
			var simulator = new Simulator(rules.arena, game, microTicks);
			MiXa.Robot robotSimulation = simulator.GetRobot(robot.Id);
			MiXa.Robot opponentSimulation = simulator.GetRobot(opponent.Id);
			robotSimulation.R = Consts.ROBOT_MAX_RADIUS;
			var robotNext = robotSimulation.Pos + robotSimulation.Speed;
			var oponentNext = opponentSimulation.Pos + opponentSimulation.Speed;
			if (useNitro) {
				robotSimulation.Action = new Struct.Action();
				robotSimulation.Action.UseNitro = true;
				robotSimulation.Action.TargetVelocity = new Point3(0, 0, 1000);
			}
			var tickLimit = 8;
			while (simulator.Ticks < tickLimit && robotNext.Dist(oponentNext) > Consts.ROBOT_RADIUS + Consts.ROBOT_MAX_RADIUS) {
				if (simulator.Ticks > 0 && robotNext.Dist(oponentNext) > robotSimulation.Pos.Dist(oponentNext)) {
					return null;
				}
				simulator.Update();
				robotSimulation.R = Consts.ROBOT_RADIUS;
				robotNext = robotSimulation.Pos + robotSimulation.Speed;
				oponentNext = opponentSimulation.Pos + opponentSimulation.Speed;
			}
			var result = simulator.Ticks < tickLimit || robotNext.Dist(oponentNext) < Consts.ROBOT_RADIUS + Consts.ROBOT_MAX_RADIUS ? simulator : null;
			return result;
		}

		private Impact GetImpact(MiXa.Robot robot, MiXa.Entity entity) {
			var robotNext = robot.Pos + robot.Speed;
			var entityNext = entity.Pos + entity.Speed;
			var d = robot.Pos.Dist(entity.Pos) - Consts.ROBOT_MAX_RADIUS - entity.R;
			var D = robot.Pos.Dist(entity.Pos) - robotNext.Dist(entityNext);
			if (D.Eq(0)) {
				return null;
			}
			var k = d / D;
			var entityPosAtImpact = entity.Pos + entity.Speed * k;
			var robotPosAtImpact = robot.Pos + robot.Speed * k;
			robotPosAtImpact.Z -= Consts.GRAVITY * k * k / 2;
			var impactPos = entityPosAtImpact + (robotPosAtImpact - entityPosAtImpact).Normalize() * entity.R;
			return new Impact {
				robot = robotPosAtImpact,
				ball = entityPosAtImpact,
				impact = impactPos
			};
		}

		private void CheckDefensiveJump(MiXa.Robot robot) {
			var useNitro = true;
			for (var i = 0; i < 2; i++) {
				var simulator = CanHitBall(robot, Consts.ROBOT_MAX_RADIUS, useNitro);
				if (simulator != null) {
					var robotSimulation = simulator.GetRobot(robot.Id);
					var impact = GetImpact(robotSimulation, simulator.Ball);
					if (impact != null) {
						if (!useNitro && robot.CanUseNitro && impact.ball.Z - impact.robot.Z > (Consts.BALL_RADIUS + Consts.ROBOT_RADIUS) * 0.9) {
							impact = null;
						}
					}
					if (impact != null && impact.robot.Z < impact.ball.Z - Consts.BALL_RADIUS / 2 && impact.robot.Y < impact.ball.Y + Consts.ROBOT_RADIUS / 10) {
						robot.Action.JumpSpeed = Consts.ROBOT_MAX_JUMP_SPEED;
						if (robotSimulation.Action != null && robotSimulation.Action.TargetVelocity.Z == 1000) {
							useNitroTill[robot.Id] = tick + Math.Min(Consts.MAX_NITRO_TICKS - 1, simulator.Ticks);
						}
						return;
					}
				}
				useNitro = !useNitro;
			}
		}

		private void CheckOffensiveJump(MiXa.Robot robot, int tickLimit = 80) {
			var useNitro = true;
			for (var i = 0; i < 2; i++) {
				var dr = 0.017;
				var rMin = Consts.ROBOT_MIN_RADIUS + dr;
				var r = Consts.ROBOT_MAX_RADIUS;
				while (r.More(rMin)) {
					var simulator = CanHitBall(robot, r, useNitro);
					if (simulator == null) {
						//break;
					} else {
						if (!robot.Touch) {
							if (simulator.Ticks == 0) {
								robot.Action.JumpSpeed = Consts.ROBOT_MAX_JUMP_SPEED;
							}
							return;
						}
						var simulator2 = new Simulator(rules.arena, simulator.Robots, simulator.Ball, simulator.MicroTicksCount);
						if (CanScore(robot, simulator2, Consts.ROBOT_MAX_RADIUS, tickLimit)) {
							if (simulator.Ticks > 0) {
								robot.Action.JumpSpeed = simulator.GetRadiusChangeSpeed(r) * Consts.TICKS_PER_SECOND;
								var robotSimulation = simulator.GetRobot(robot.Id);
								if (robotSimulation.Action != null && robotSimulation.Action.TargetVelocity.Z == 1000) {
									useNitroTill[robot.Id] = tick + Math.Min(Consts.MAX_NITRO_TICKS - 1, simulator.Ticks);
								}
							} else {
								robot.Action.JumpSpeed = Consts.ROBOT_MAX_JUMP_SPEED;
							}
							if (render) {
								Console.WriteLine(robot.Action.JumpSpeed);
							}
							return;
						}
						// Always jump at faceoff
						if (robot.Touch && simulator.Ticks == 0 && ball.X.Eq(0) && ball.Y.Eq(0)) {
							robot.Action.JumpSpeed = Consts.ROBOT_MAX_JUMP_SPEED;
							return;
						}
					}
					r -= dr;
				}
				useNitro = !useNitro;
			}
		}

		private void CheckJump(MiXa.Robot robot) {
			if (robot.Action.JumpSpeed.More(0)) {
				return;
			}
			if (robot.Id == goalkeeperId) {
				if (robot.Touch || robot.Pos.Dist(ball.Pos) < ball.R * 2.5) {
					timers[0].Start();
					CheckOffensiveJump(robot, 120);
					timers[0].Stop();
				}
				if (robot.Action.JumpSpeed.Eq(0)) {
					if (robot.Touch) {
						CheckDefensiveJump(robot);
					} else {
						if (robot.Pos.Dist(ball.Pos) < ball.R * 2.5) {
							robot.Action.JumpSpeed = Consts.ROBOT_MAX_JUMP_SPEED;
						}
					}
				}
			} else {
				if (robot.Touch || robot.Pos.Dist(ball.Pos) < ball.R * 2.5) {
					timers[1].Start();
					CheckOffensiveJump(robot);
					timers[1].Stop();
				}
				if (robot.Action.JumpSpeed.More(0)) {
					return;
				}
				if (robot.Y < 0) {
					if (robot.Touch) {
						var closestOpponent = ClosestOpponentInFront(robot.Pos);
						if (closestOpponent != null && closestOpponent.Pos2.Dist(ball.Pos2).Less(ball.R * 6)) {
							CheckDefensiveJump(robot);
						} else if (ball.Y.Less(myGoal.Y + ball.R)) {
							CheckDefensiveJump(robot);
						}
					} else {
						if (robot.Pos.Dist(ball.Pos).Less(ball.R * 2.5)) {
							robot.Action.JumpSpeed = Consts.ROBOT_MAX_JUMP_SPEED;
						}
					}
				}
			}
		}

		private Point2 GetOffensiveTargetPoint(MiXa.Robot robot, MiXa.Ball simulatedBall) {
			var targetAtGoal = hisGoal + new Point2(0, Consts.BALL_RADIUS);
			if (simulatedBall.Y + 2 * Consts.BALL_RADIUS > hisGoal.Y) {
				targetAtGoal += new Point2(0, Consts.BALL_RADIUS) * 2;
			}
			var vectorFromTargetLength = Consts.BALL_RADIUS;
			if (robot.Y > simulatedBall.Y) {
				vectorFromTargetLength += Consts.ROBOT_RADIUS * 0.75;
			}
			var vectorFromTarget = simulatedBall.Pos2 - targetAtGoal; // Vector from his goal to the ball
			if (simulatedBall.Speed.Y > 0.2) {
				var dY = targetAtGoal.Y - simulatedBall.Y;
				var t = dY / simulatedBall.Speed.Y;
				var x = simulatedBall.X + simulatedBall.Speed.X * t;
				var dX = Math.Abs(x - simulatedBall.X);
				var dist = (new Point2(x, targetAtGoal.Y) - simulatedBall.Pos2).Dist();
				var sinAngle = dX / dist;
				var cosAngle = dY / dist;
				if (x > 0) {
					sinAngle = -sinAngle;
				}
				vectorFromTarget = new Point2(vectorFromTarget.X * cosAngle + vectorFromTarget.Y * sinAngle, vectorFromTarget.Y * cosAngle - vectorFromTarget.X * sinAngle);
			}
			vectorFromTarget = vectorFromTarget.Normalize() * vectorFromTargetLength;
			var targetPoint = simulatedBall.Pos2 + vectorFromTarget;
			return targetPoint;
		}

		private MiXa.NitroPack CanPickupNitroPack(MiXa.Robot robot) {
			if (nitroPacks.Length == 0) {
				return null;
			}
			if (robot.Id == goalkeeperId) {
				if (robot.NitroAmount > Consts.MAX_NITRO_AMOUNT / 2.1) {
					return null;
				}
				if (ball.Speed.Y < 0) {
					return null;
				}
			}

			var ballTick = tick;
			var snapshot = simulation.GetTick(ballTick);
			while (snapshot != null) {
				ballTick++;
				snapshot = simulation.GetTick(ballTick);
				if (snapshot == null) {
					break;
				}
				if (snapshot.Ball.Z < jumpHeights[jumpHeights.Length - 1] + Consts.BALL_RADIUS + 2.5 * (robot.Id == middleId ? 0 : Math.Sign(robot.NitroAmount))) {
					break;
				}
			}
			if (ballTick - tick < 5) {
				//return null;
			}
			if (snapshot == null) {
				ballTick--;
				snapshot = simulation.GetTick(ballTick);
			}
			var tickLimit = (int)(ballTick - tick + snapshot.Ball.Pos2.Dist(robot.Id == middleId ? hisGoal : myGoal) / (Consts.MAX_ENTITY_SPEED * 0.7)) - 50;
			var packs = nitroPacks.OrderBy(x => robot.Pos2.Dist(x.Pos2)).Take(2);
			while (packs.Count() > 0) {
				var pack = packs.First();
				packs = packs.Skip(1);
				if (pack.RespawnTicks < tickLimit) {
					var tickToReach = TickToReach(robot, pack.Pos, tickLimit, 3);
					var canReach = tickToReach.Item1 <= tickLimit;
					if (canReach) {
						return pack;
					}
				}
			}
			return null;
		}

		private void CheckHitOpponent(MiXa.Robot robot) {
			var closestToRobotOpponent = ClosestOpponent(robot.Pos);
			if (!closestToRobotOpponent.Touch) {
				var simulator = CanHitOpponent(robot, closestToRobotOpponent);
				if (simulator != null) {
					var robotSimulation = simulator.GetRobot(robot.Id);
					var opponentSimulation = simulator.GetRobot(closestToRobotOpponent.Id);
					var impact = GetImpact(robotSimulation, opponentSimulation);
					if (impact != null && impact.impact.Z > impact.robot.Z + Consts.ROBOT_RADIUS / 2) {
						robot.Action.JumpSpeed = Consts.ROBOT_MAX_JUMP_SPEED;
						if (robotSimulation.Action != null && robotSimulation.Action.TargetVelocity.Z == 1000) {
							useNitroTill[robot.Id] = tick + simulator.Ticks;
						}
						if (robot.Touch && render) {
							Console.WriteLine(tick + " aw");
						}
					}
				}
			}
		}

		private void Act(MiXa.Robot robot) {
			if (actionsStore.ContainsKey(robot.Id) && actionsStore[robot.Id].GetAction(tick) != null && !robot.Pos.CloseTo(actionsStore[robot.Id].GetAction(tick).state.Pos)) {
				actionsStore.Remove(robot.Id);
			}
			if (robot.Id != goalkeeperId && actionsStore.ContainsKey(goalkeeperId) && actionsStore[goalkeeperId].GetAction(tick + 1) != null) {
				actionsStore.Remove(robot.Id);
			}
			if (actionsStore.ContainsKey(robot.Id) && actionsStore[robot.Id].GetAction(tick) != null) {
				robot.Action = actionsStore[robot.Id].GetAction(tick).action;
				if (robot.Action.JumpSpeed.Eq(0)) {
					var robotNext = robot.Pos + robot.Speed;
					var ballNext = ball.Pos + ball.Speed;
					if (robot.Id == goalkeeperId && robotNext.Dist(ballNext) > Consts.BALL_RADIUS + Consts.ROBOT_MAX_RADIUS) {
						timers[2].Start();
						CheckOffensiveJump(robot, 120);
						timers[2].Stop();
					} else {
						CheckJump(robot);
					}
				}
			} else {
				Go(robot);
			}
			if (useNitroTill.ContainsKey(robot.Id) && useNitroTill[robot.Id] >= tick) {
				robot.Action.UseNitro = true;
				robot.Action.TargetVelocity = new Point3(0, 0, 1000);
			}
		}

		public void Act(Model.Robot _me, Rules _rules, Game _game, Model.Action _action) {
			Init(_me, _rules, _game, _action);

			if (timeOut) {
				var robot = myRobots.Where(x => x.Id == me.Id).First();
				robot.Action = new Struct.Action();
				GoTo(robot, new Point2(myRobots.Average(x => x.X), myRobots.Average(x => x.Y)));
				SetAction(robot);
				return;
			}

			timer.Start();
			if (tickFirstRun) {
				var robotNumber = -1;
				foreach (var robot in myRobots) {
					robotNumber++;
					canHitBallStore = new Dictionary<double, Dictionary<bool, Simulator>>();
					robot.Action = new Struct.Action();
					timersRobots[robotNumber].Start();
					Act(robot);
					timersRobots[robotNumber].Stop();
				}
			}
			SetAction(myRobots.Where(x => x.Id == me.Id).First());
			timer.Stop();
		}

		private void Go(MiXa.Robot robot) {
			if (robot.Id != middleId) {
				CheckJump(robot);
			}
			if (robot.Id == goalkeeperId) {
				GoGoalkeeper(robot);
			} else if (robot.Id == middleId) {
				GoMiddle(robot);
			} else {
				GoForward(robot);
			}
		}

		private void GoGoalkeeper(MiXa.Robot robot) {
			if (robot.Action.JumpSpeed.More(0)) {
				return;
			}

			var nitroPackForPickup = CanPickupNitroPack(robot);
			if (nitroPackForPickup != null) {
				GoTo(robot, nitroPackForPickup.Pos2, true);
				return;
			}

			var target = myGoal;
			if (target.Y > ballNext.Y - Consts.ROBOT_RADIUS) {
				target.Y = ballNext.Y - Consts.ROBOT_RADIUS;
			}
			var ballIsIntercepted = false;
			if (robot.Touch) {
				var closestOpponent = ClosestOpponent(ball.Pos);
				var closestOpponentNextPos = closestOpponent.Pos + closestOpponent.Speed * 2;
				var relativeSpeed = closestOpponent.Speed2 - ball.Speed2;
				var relativeDist = ball.Pos2 - closestOpponent.Pos2;
				var speed = relativeSpeed * relativeDist;
				if (speed.Eq(0)) {
					speed = Consts.EPS;
				}
				var closestOpponentTickToReach = relativeDist.Dist() / speed;
				var ballNextPos = ball.Pos + ball.Speed * 2;
				var closestOpponentCloseToBall = closestOpponentNextPos.Dist(ballNextPos) < (Consts.BALL_RADIUS + Consts.ROBOT_RADIUS) * 2;
				var closestOpponentIsOnTheGround = closestOpponent.Z < Consts.ROBOT_MAX_RADIUS * 1.1;
				if (closestOpponentCloseToBall && closestOpponentIsOnTheGround && !IsInGoalMouth(ball.Pos)) {
					// Do not intercept
				} else {
					var tickLimitToIntercept = 60;
					var ballTick = GetTickToStartAction(robot.Id, "Intercept") - 1;
					if (ballTick < tick) {
						ballTick = tick;
					};
					var snapshot = simulation.GetTick(ballTick);
					while (snapshot != null && ballTick < tick + tickLimitToIntercept && !ballIsIntercepted) {
						while (ballTick < tick + tickLimitToIntercept) {
							ballTick++;
							snapshot = simulation.GetTick(ballTick);
							if (snapshot == null) {
								break;
							}
							if (IsInGoalMouth(snapshot.Ball.Pos) && snapshot.Ball.Z < jumpHeights[jumpHeights.Length - 1] + Consts.BALL_RADIUS + Consts.ROBOT_RADIUS + 1.5 * Math.Sign(robot.NitroAmount)) {
								break;
							}
						}
						if (ballTick < tick + tickLimitToIntercept && snapshot != null) {
							var targetPoint = snapshot.Ball.Pos - new Point3(0, Consts.ROBOT_MAX_RADIUS, Consts.ROBOT_MAX_RADIUS);
							if (Math.Abs(snapshot.Ball.Pos.X) < rules.arena.goal_width / 2 && snapshot.Ball.Y < -rules.arena.depth / 2 + Consts.BALL_RADIUS * 2) { // ball is almost in my goal
								targetPoint = snapshot.Ball.Pos - new Point3(0, Consts.ROBOT_RADIUS * 1, Consts.ROBOT_RADIUS * 2.5);
							} else if (Math.Abs(snapshot.Ball.Pos.X) > rules.arena.goal_width / 2) { // ball is to the side
								targetPoint = snapshot.Ball.Pos - new Point3(0, Consts.ROBOT_MAX_RADIUS, Consts.ROBOT_MAX_RADIUS);
							} else if (snapshot.Ball.Pos.Z < Consts.BALL_RADIUS + Consts.ROBOT_RADIUS) { // ball is very low
								targetPoint = snapshot.Ball.Pos - new Point3(0, Consts.BALL_RADIUS + Consts.ROBOT_RADIUS / 2, Consts.ROBOT_RADIUS);
							} else if (snapshot.Ball.Pos.Z < jumpHeights[jumpHeights.Length - 1]) { // ball is low enough
								targetPoint = snapshot.Ball.Pos - new Point3(0, Consts.BALL_RADIUS, Consts.ROBOT_RADIUS);
							}
							targetPoint.Z = Math.Max(Consts.ROBOT_MAX_RADIUS, targetPoint.Z);
							var tickToReach = TickToReach(robot, targetPoint, ballTick - tick);
							var canReach = tickToReach.Item1 <= ballTick - tick;
							if (canReach) {
								actionsStore.Clear();
								actionsStore[robot.Id] = new ActionsList(tick, tickToReach.Item2);
								ballIsIntercepted = true;
								if (actionsStore[robot.Id].GetAction(tick) != null) {
									robot.Action = actionsStore[robot.Id].GetAction(tick).action;
								}
								return;
							} else {
								SetTickToStartAction(robot.Id, "Intercept", ballTick + 1);
							}
						}
					}
					if (snapshot == null || ballTick == tick + tickLimitToIntercept) {
						SetTickToStartAction(robot.Id, "Intercept", ballTick);
					}
				}
			}
			if (!ballIsIntercepted) {
				if (ball.Speed.Y < -0.15) {
					var t = (target.Y - ball.Y) / ball.Speed.Y; // Time ball needs to get to my goal
					var x = ball.X + ball.Speed.X * t; // X coord of the ball in my goal
					if (Math.Abs(x) < (rules.arena.goal_width / 2)) {
						target.X = x;
					} else {
						target.X = ball.X.Clamp(-rules.arena.goal_width / 3, rules.arena.goal_width / 3);
					}
				} else if (ball.Y < myGoal.Y + ball.R * 3) {
					if (Math.Abs(ballNext.X + ball.Speed.X * 2) < (rules.arena.goal_width / 2)) {
						target.X = ballNext.X + ball.Speed.X * 2;
					} else {
						target.X = (ballNext.X + ball.Speed.X * 2).Clamp(-rules.arena.goal_width / 2, rules.arena.goal_width / 2);
					}
				} else {
					target.X = ballNext.X.Clamp(-rules.arena.goal_width / 4, rules.arena.goal_width / 4);
				}
				target.X = target.X.Clamp(-rules.arena.goal_width / 2 + rules.arena.bottom_radius, rules.arena.goal_width / 2 - rules.arena.bottom_radius);
				var closestOpponent = ClosestOpponentInFront(robot.Pos);
				if (closestOpponent != null && robot.Pos.Dist(closestOpponent.Pos) < Consts.ROBOT_RADIUS * 4) {
					target.Y += Consts.ROBOT_RADIUS * 4;
				}
				GoTo(robot, target, true);
			}
		}

		private void GoForward(MiXa.Robot robot) {
			var goalkeeper = myRobots.Where(x => x.Id == goalkeeperId).First();
			var goalkeeperIsInAction = actionsStore.ContainsKey(goalkeeperId) && actionsStore[goalkeeperId].GetAction(tick) != null;
			var goalkeeperIsJumping = goalkeeper.Speed.Y > 0 && goalkeeper.Speed.Z > 0.1;
			var goalkeeperBlocker = ClosestOpponentInFront(goalkeeper.Pos);
			var goalkeeperIsBlocked = goalkeeperBlocker == null ? false : goalkeeper.Pos.Dist(goalkeeperBlocker.Pos) < Consts.ROBOT_RADIUS * 5;
			if (robot.Touch && !ball.Pos2.Eq(new Point2(0, 0)) && (goalkeeperIsInAction || goalkeeperIsJumping) && !goalkeeperIsBlocked) {
				if (goalkeeperIsJumping || goalkeeper.Speed.Dist() > Consts.ROBOT_MAX_GROUND_SPEED / 5) {
					robot.Action.JumpSpeed = 0;
					useNitroTill[robot.Id] = 0;
					lastJump = tick;
				}
				var ballPos = ball.Pos;
				if (goalkeeperIsInAction) {
					var snapshotToGo = simulation.GetTick(actionsStore[goalkeeperId].LastTick);
					if (snapshotToGo != null) {
						ballPos = snapshotToGo.Ball.Pos;
					}
				}
				var opponent = ClosestOpponent(ballPos);
				if (robot.Id == middleId) {
					opponent = ClosestOpponent(goalkeeper.Pos);
				}

				CheckHitOpponent(robot);

				var ticksToReachOpponent = (robot.Pos2.Dist(opponent.Pos2) - Consts.ROBOT_RADIUS * 2) / (robot.Speed2.Dist() + opponent.Speed2.Dist() + Consts.EPS);
				ticksToReachOpponent = ticksToReachOpponent.Clamp(2, 100);
				var targetPoint = opponent.Pos2 + opponent.Speed2 * ticksToReachOpponent + new Point2(0, -0.1) * ticksToReachOpponent.Clamp(0, 10);
				var xMax = rules.arena.width / 2 - rules.arena.bottom_radius - Consts.ROBOT_RADIUS * 2;
				targetPoint.X = targetPoint.X.Clamp(-xMax, xMax);
				targetPoint.Y = Math.Min(0, targetPoint.Y);
				GoTo(robot, targetPoint, false);
				return;
			}
			if (ball.Pos2.Eq(new Point2(0, 0)) && tick - lastGoal < 15) {
				GoTo(robot, new Point2(0, robot.Y), true);
				return;
			}
			if (ball.Pos2.Eq(new Point2(0, 0)) && robot.Pos2.Dist(ball.Pos2) < 12 && robot.Pos2.Dist(ball.Pos2) > Consts.BALL_RADIUS * 2 && robot.Speed.Dist() > Consts.ROBOT_MAX_GROUND_SPEED / 5) {
				var opponent = ClosestOpponent(ball.Pos);
				var myDist = robot.Pos2.Dist(ball.Pos2);
				var hisDist = opponent.Pos2.Dist(ball.Pos2);
				if (myDist < hisDist + Consts.ROBOT_RADIUS / 2 && myDist + Consts.ROBOT_RADIUS > hisDist) {
					return;
				}
			}

			if (!robot.Touch) {
				return;
			}

			if (robot.Action.JumpSpeed.More(0)) {
				return;
			}

			var closestNitroPack = nitroPacks.OrderBy(x => robot.Pos.Dist(x.Pos)).FirstOrDefault();
			if (closestNitroPack != null && me.NitroAmount < Consts.MAX_NITRO_AMOUNT / 2 && robot.Pos.Dist(closestNitroPack.Pos) < Consts.ROBOT_RADIUS * 5 && closestNitroPack.RespawnTicks < 10) {
				GoTo(robot, closestNitroPack.Pos2, false);
				return;
			}

			var ballTick = GetTickToStartAction(robot.Id, "PickTheBall") - 1;
			if (ballTick < tick) {
				ballTick = tick;
			};
			var snapshot = simulation.GetTick(ballTick);
			while (snapshot != null) {
				ballTick++;
				snapshot = simulation.GetTick(ballTick);
				if (snapshot == null) {
					break;
				}
				if (snapshot.Ball.Z < jumpHeights[jumpHeights.Length - 1] + Consts.BALL_RADIUS + 2.5 * Math.Sign(robot.NitroAmount)) {
					break;
				}
			}
			if (snapshot != null) {
				var targetPoint = new Point3(GetOffensiveTargetPoint(robot, snapshot.Ball));
				targetPoint.Z = snapshot.Ball.Z - Consts.ROBOT_RADIUS;
				if (render) {
					renderSphere.Add(new Sphere(targetPoint, 1.5, 0, 0, 1, 0.8));
				}
				var tickToReach = TickToReach(robot, targetPoint, ballTick - tick);
				var canReach = tickToReach.Item1 <= ballTick - tick;
				if (canReach) {
					actionsStore[robot.Id] = new ActionsList(tick, tickToReach.Item2);
					if (actionsStore[robot.Id].GetAction(tick) != null) {
						robot.Action = actionsStore[robot.Id].GetAction(tick).action;
					}
					return;
				} else {
					while (snapshot != null && snapshot.Ball.Z > Consts.BALL_RADIUS + Consts.ROBOT_RADIUS * 2) {
						ballTick++;
						snapshot = simulation.GetTick(ballTick);
					}
					if (snapshot == null) {
						ballTick--;
						snapshot = simulation.GetTick(ballTick);
					}
					targetPoint = new Point3(GetOffensiveTargetPoint(robot, snapshot.Ball));
					targetPoint.Z = snapshot.Ball.Z - Consts.ROBOT_RADIUS;
					GoTo3(robot, targetPoint);
					SetTickToStartAction(robot.Id, "PickTheBall", ballTick);
					return;
				}
			} else {
				while (snapshot == null) {
					ballTick--;
					snapshot = simulation.GetTick(ballTick);
				}
				var targetPoint = new Point3(GetOffensiveTargetPoint(robot, snapshot.Ball));
				targetPoint.Z = snapshot.Ball.Z - Consts.ROBOT_RADIUS;
				GoTo3(robot, targetPoint);
				SetTickToStartAction(robot.Id, "PickTheBall", ballTick);
				return;
			}
		}

		private void GoMiddle(MiXa.Robot robot) {
			if (robot.Y < 0 && tick - lastJump > 15) {
				CheckDefensiveJump(robot);
				if (robot.Action.JumpSpeed > 0) {
					if (robot.Touch && render) {
						Console.WriteLine(tick.ToString() + " mj");
					}
					return;
				}
			}
			var iAmLeading = (game.players.Where(x => x.id == me.PlayerId).First().score - game.players.Where(x => x.id != me.PlayerId).First().score - 1) * 700 >= rules.max_tick_count - tick;
			if (!iAmLeading) {
				if (robot.NitroAmount > Consts.MAX_NITRO_AMOUNT * 0.99 || ball.Y > 0 && ball.Speed.Y > Consts.ROBOT_MAX_GROUND_SPEED * 0.6 && me.Speed2.Dist() < Consts.ROBOT_MAX_GROUND_SPEED * 0.9) {
					robot.Action.UseNitro = true;
				}
				var nitroPackForPickup = CanPickupNitroPack(robot);
				if (nitroPackForPickup != null) {
					GoTo(robot, nitroPackForPickup.Pos2, true);
					return;
				}
			} else {
				if (ball.Pos2.Eq(new Point2(0, 0)) && tick - lastGoal < 25) {
					GoTo(robot, new Point2(0, robot.Y - Consts.ROBOT_RADIUS), true);
					return;
				}
				var closestNitroPack = nitroPacks.OrderBy(x => robot.Pos.Dist(x.Pos)).First();
				if (me.NitroAmount < Consts.MAX_NITRO_AMOUNT && robot.Pos.Dist(closestNitroPack.Pos) < Consts.ROBOT_RADIUS * 5 && closestNitroPack.RespawnTicks < 10) {
					GoTo(robot, closestNitroPack.Pos2, false);
					return;
				}
				if (ball.Y < 0 && ball.Speed.Y < -Consts.ROBOT_MAX_GROUND_SPEED * 0.6 && me.Speed2.Dist() < Consts.ROBOT_MAX_GROUND_SPEED * 0.9) {
					robot.Action.UseNitro = true;
				}
			}

			CheckHitOpponent(robot);

			var opponent = ClosestOpponent(new Point3(hisGoal));
			if (iAmLeading) {
				var goalkeeper = myRobots.Where(x => x.Id == goalkeeperId).First();
				var opponents = hisRobots.OrderBy(x => goalkeeper.Pos2.Dist(x.Pos2 + x.Speed2 * 10));
				opponent = opponents.First();
				if (opponent.Pos2.Dist(goalkeeper.Pos2) < Consts.ROBOT_RADIUS * 4) {
					opponent = opponents.Skip(1).First();
				}
			}

			var ticksToReachOpponent = (robot.Pos2.Dist(opponent.Pos2) - Consts.ROBOT_RADIUS * 2) / (robot.Speed2.Dist() + opponent.Speed2.Dist() + Consts.EPS);
			ticksToReachOpponent = ticksToReachOpponent.Clamp(1, 10);
			var targetPoint = opponent.Pos2 + opponent.Speed2 * ticksToReachOpponent;
			var ballTick = tick;
			var snapshot = simulation.GetTick(ballTick);
			while (snapshot != null && snapshot.Ball.Y < rules.arena.depth / 2 - Consts.BALL_RADIUS * 3 && snapshot.Ball.Z > 8) {
				ballTick++;
				snapshot = simulation.GetTick(ballTick);
			}
			while (snapshot == null) {
				ballTick--;
				snapshot = simulation.GetTick(ballTick);
			}
			var vectorToBall = snapshot.Ball.Pos2 - targetPoint;
			targetPoint += vectorToBall.Normalize() * Consts.ROBOT_RADIUS * (2 - 4 * opponent.Speed2.Dist().Clamp(0.02, 0.4));
			if (!iAmLeading) {
				var xMax = rules.arena.width / 2 - rules.arena.bottom_radius - Consts.ROBOT_RADIUS / 2;
				var yMax = rules.arena.depth / 2 + rules.arena.goal_depth - rules.arena.bottom_radius - Consts.ROBOT_RADIUS;
				targetPoint.Y = targetPoint.Y.Clamp(-yMax, yMax);
				if (targetPoint.Y > rules.arena.depth / 2 - rules.arena.bottom_radius) {
					xMax = rules.arena.goal_width / 2 - rules.arena.bottom_radius - Consts.ROBOT_RADIUS / 2;
				}
				targetPoint.X = targetPoint.X.Clamp(-xMax, xMax);
			}
			GoTo(robot, targetPoint, opponent.Speed2.Dist() < Consts.ROBOT_MAX_GROUND_SPEED / 4);
		}

		private void SetTickToStartAction(int id, string key, int tickToStart) {
			if (!tickToStartAction.ContainsKey(id)) {
				tickToStartAction[id] = new Dictionary<string, int>();
			}
			tickToStartAction[id][key] = tickToStart;
		}

		private int GetTickToStartAction(int id, string key) {
			if (!tickToStartAction.ContainsKey(id)) {
				return 0;
			}
			if (!tickToStartAction[id].ContainsKey(key)) {
				return 0;
			}
			return tickToStartAction[id][key];
		}

		private void SetAction(MiXa.Robot robot) {
			var targetVelocity = robot.Action.TargetVelocity;
			action.target_velocity_x = targetVelocity.X * Consts.TICKS_PER_SECOND;
			action.target_velocity_y = targetVelocity.Z * Consts.TICKS_PER_SECOND;
			action.target_velocity_z = targetVelocity.Y * Consts.TICKS_PER_SECOND;
			action.jump_speed = robot.Action.JumpSpeed;
			action.use_nitro = robot.Action.UseNitro;
			if (robot.Touch && action.jump_speed > 0) {
				lastJump = tick;
			}
		}

		public string CustomRendering() {
			var json = new List<string>();
			if (render) {
				var goalMouthR = rules.arena.goal_width / 2 + ball.R;
				renderSphere.Add(new Sphere(myGoal.X - 5, myGoal.Y + ball.R, ball.R, goalMouthR, 0, 1, 0, 0.3));
				renderSphere.Add(new Sphere(myGoal.X, myGoal.Y + ball.R, ball.R, goalMouthR, 0, 1, 0, 0.3));
				renderSphere.Add(new Sphere(myGoal.X + 5, myGoal.Y + ball.R, ball.R, goalMouthR, 0, 1, 0, 0.3));
				foreach (var figure in renderText) {
					json.Add(figure.Json());
				}
				foreach (var figure in renderSphere) {
					json.Add(figure.Json());
				}
				foreach (var figure in renderLine) {
					json.Add(figure.Json());
				}
			}
			return "[" + String.Join(",", json) + "]";
		}
	}
}
