using Struct;
using Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk.Model;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk.Model.MiXa {
	public class Entity {
		public double X {get; set;}
        public double Y {get; set;}
        public double Z {get; set;}
		public Point3 Speed {get; set;}
        public double R {get; set;}
		public double M {get;}

		public Point3 Pos {
			get {return new Point3(X, Y, Z);}
			set {X = value.X; Y = value.Y; Z = value.Z;}
		}
		public Point2 Pos2 {get {return new Point2(X, Y);}}
		public Point3 Pos3 {get {return Pos;}}

		public Point2 Speed2 {get {return new Point2(Speed.X, Speed.Y);}}
		public Point3 Speed3 {get {return Speed;}}

		private void Constructor(double x, double y, double z, double sx, double sy, double sz, double r) {
			X = x;
			Y = y;
			Z = z;
			Speed = new Point3(sx, sy, sz);
			R = r;
		}

		public Entity(Model.Ball entity) {
			Constructor(entity.x, entity.z, entity.y, entity.velocity_x / Consts.TICKS_PER_SECOND, entity.velocity_z / Consts.TICKS_PER_SECOND, entity.velocity_y / Consts.TICKS_PER_SECOND, entity.radius);
			M = Consts.BALL_MASS;
		}

		public Entity(Model.Robot entity) {
			Constructor(entity.x, entity.z, entity.y, entity.velocity_x / Consts.TICKS_PER_SECOND, entity.velocity_z / Consts.TICKS_PER_SECOND, entity.velocity_y / Consts.TICKS_PER_SECOND, entity.radius);
			M = Consts.ROBOT_MASS;
		}

		public Entity(Model.NitroPack entity) {
			Constructor(entity.x, entity.z, entity.y, 0, 0, 0, entity.radius);
			M = 0;
		}

		public Entity(Entity entity) {
			Constructor(entity.X, entity.Y, entity.Z, entity.Speed.X, entity.Speed.Y, entity.Speed.Z, entity.R);
			M = entity.M;
		}
	}
}