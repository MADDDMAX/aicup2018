﻿using System;
using System.Collections.Generic;
using System.Linq;
using Struct;
using Ext;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk {
	class Simulation {
		private Simulator simulator;
		private int myPlayerId;
		private Model.Rules rules;
		private List<Snapshot> ticks;
		private int startTick = 0;
		private int endTick = -1;
		public int TicksCount;

		public Simulation(int myPlayerId, Model.Rules rules, int ticksCount = 120) {
			TicksCount = ticksCount;
			ticks = new List<Snapshot>(ticksCount);
			this.myPlayerId = myPlayerId;
			this.rules = rules;
		}

		private bool GoalScored(double y = 1000) {
			if (y == 1000) {
				y = simulator.Ball.Y;
			}
			return Math.Abs(y) > rules.arena.depth / 2 + Consts.BALL_RADIUS;
		}

		private bool NeedReset(Model.Game game) {
			if (GoalScored(game.ball.z)) {
				return false;
			}
			var snapshot = GetTick(game.current_tick);
			if (snapshot == null) {
				return true;
			}
			if (!game.ball.x.Eq(snapshot.Ball.X) || !game.ball.z.Eq(snapshot.Ball.Y) || !game.ball.y.Eq(snapshot.Ball.Z)) {
				return true;
			}
			if (snapshot.Robots.Length == 0) {
				return true;
			}
			for (int i = 0; i < game.robots.Length; i++) {
				for (int j = 0; j < snapshot.Robots.Length; j++) {
					if (game.robots[i].id == snapshot.Robots[j].Id) {
						if (game.robots[i].touch != snapshot.Robots[j].Touch) {
							return true;
						}
						var robotPos = new Point3(game.robots[i].x, game.robots[i].z, game.robots[i].y);
						if (robotPos.Dist(snapshot.Ball.Pos) < Consts.BALL_RADIUS * 4 && !robotPos.CloseTo(snapshot.Robots[j].Pos, Consts.ROBOT_RADIUS / 20)) {
							return true;
						}
						break;
					}
				}
			}
			return false;
		}

		private void Reset(Model.Game game) {
			startTick = game.current_tick;
			endTick = startTick - 1;
			var myId = game.players.Where(x => x.me).Select(x => x.id).First();
			var robots = game.robots.Where(x => x.player_id != myId).Select(x => new Model.MiXa.Robot(x)).ToArray();
			simulator = new Simulator(rules.arena, robots, new Model.MiXa.Ball(game.ball), 100);
			ticks.Clear();
		}

		public bool Update(Model.Game game) {
			if (NeedReset(game)) {
				Reset(game);
				for (int i = 0; i < TicksCount; i++) {
					AddTick();
					if (GoalScored()) {
						break;
					}
					if (i == 30) {
						simulator.RemoveRobots();
					}
				}
				return true;
			} else {
				AddTick();
				return false;
			}
		}

		public void AddTick() {
			if (GoalScored()) {
				return;
			}
			ticks.Add(new Snapshot(simulator.Ball, simulator.Robots));
			endTick++;
			var ballNextPos = simulator.Ball.Pos + simulator.Ball.Speed;
			for (int j = 0; j < simulator.Robots.Length; j++) {
				var robotNextPos = simulator.Robots[j].Pos + simulator.Robots[j].Speed;
				if (!simulator.Robots[j].Touch && robotNextPos.Dist(ballNextPos).Less(Consts.BALL_RADIUS + Consts.ROBOT_MAX_RADIUS)) {
					simulator.Robots[j].R = Consts.ROBOT_MAX_RADIUS;
				} else {
					simulator.Robots[j].R = Consts.ROBOT_MIN_RADIUS;
				}
			}
			simulator.Update();
		}

		public Snapshot GetTick(int tick) {
			if (tick >= startTick && tick <= endTick) {
				return ticks[tick - startTick];
			}
			return null;
		}
    }
}
