using System;
using System.Collections.Generic;
using Struct;

namespace Ext {
	public static class MathMiXa {
		public static double Eps = 1e-5;
		public static Point2 ZeroPoint2 = new Point2(0, 0);

		public static double SSignedDoubled(Point2 p1, Point2 p2, Point2 p3) {
			return (p2.X - p1.X) * (p3.Y - p1.Y) - (p2.Y - p1.Y) * (p3.X - p1.X);
		}

		public static double Determinant(double a, double b, double c, double d) {
			return a * d - b * c;
		}
	}
}