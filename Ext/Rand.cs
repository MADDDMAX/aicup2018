namespace Ext {
	public sealed class Rand {
		private static System.Random random = null;

		public Rand(int seed) {
			if (random == null) {
				random = new System.Random(seed);
			}
		}
		
		// Inclusive
		public int Int(int min = 0, int max = 1000000) {
			if (random == null) {
				throw new System.Exception();
			}
			return random.Next(min, max + 1);
		}

		public double Double(double min = 0, double max = 1) {
			if (random == null) {
				throw new System.Exception();
			}
			return min + (max - min) * random.NextDouble();
		}
	}
}